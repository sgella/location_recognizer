import sys
import smithmn
from levenshtein import distance, distance_simple

#N = 3

def getNgrams(input, N):
	output = []
	for i in range(len(input)-N+1):
		output.append(input[i:i+N])
	return output

def getTargetNgramLocations(targetString, ngram_dict):
	ngrams = getNgrams(targetString, N)
	#print ngrams
	target_ngram_locations = {}
	for ngram in ngrams:
		if ngram_dict.has_key(ngram):
			for location in ngram_dict[ngram]:
				if not target_ngram_locations.has_key(location):
					target_ngram_locations[location] = 0
				target_ngram_locations[location] +=1
	return target_ngram_locations
				
def createLocationTrigrams(locations):
	# returns ngrams that exist in locations
	ngram_dict = {}
	for location in locations:
		ngrams = getNgrams(location, N)
		for ngram in ngrams:
			if not ngram_dict.has_key(ngram):
				ngram_dict[ngram] = []
			ngram_dict[ngram].append(location)
	return ngram_dict

def getOriginalFromMapped(matched_strings, targetOriginal):
	targetOriginal = targetOriginal.strip()
	for found_location in matched_strings:
		mapped = matched_strings[found_location]
		mapped = mapped.replace('-','').strip()	
		index = str.index(targetOriginal, mapped)
		start = index
		end = index
		while start>0 and targetOriginal[start-1]!=" ":
			start = start-1
		while end<len(targetOriginal)-1 and (targetOriginal[end+1]!=" " or end<index+len(mapped)-1):
			end = end+1
		matched_strings[found_location] = (targetOriginal[start:end+1])

def filterByPrepRules(targetOriginal, prepList):
	#filter target string based on rules
	words = set()
	for prep in prepList:
		if prep in targetOriginal:
			spl = targetOriginal.split(prep)
			after = spl[1]
			recursive = filterByPrepRules(after, prepList)
			if len(recursive) == 0:
				words.add(after)
			else:
				words.update(recursive)
	#print targetOriginal, words
	return words

def filterString(targetString, freqWords):
	spl = targetString.split()
	targetString = ""
	for word in spl:
		if word not in freqWords:
			targetString +=" "+word
	return targetString.strip()

def writeToFile(fout, observed_location, predicted_location, targetOriginal):
	#print "obs:", observed_location, "predicted:", predicted_location, "line::", targetOriginal
	if observed_location in targetOriginal:
		fout.write(targetOriginal.replace(observed_location, '<loc>'+predicted_location+'</loc>'))
		return True
	elif observed_location.title() in targetOriginal:
		fout.write(targetOriginal.replace(observed_location.title(), '<loc>'+predicted_location+'</loc>')) 
		return True	
			
if __name__ == "__main__":

	#parse options
	import os
	import inspect
	currentDir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
	data = os.path.join(currentDir, "../data")
			
	
	from optparse import OptionParser                         
	opts = OptionParser()
	opts.add_option("-i", "--input", dest="input", default = os.path.join(data, "Sample_Input.txt"))
	opts.add_option("-l", "--locations", dest="locations", default = os.path.join(data, "Locations.txt"))
	opts.add_option("-o", "--output", dest="output", default = os.path.join(data, "output.txt"))
	opts.add_option("-n", "--ngram", dest="n", type="int", default = 3)
	opts.add_option("-k", "--loc_count", dest="k", type="int", default = 50)
	options, args = opts.parse_args()
	
	if options.output == os.path.join(data, "output.txt"):
		print "By default output is written to output.txt in data folder"		

	global N
	N = options.n
	LOC_COUNT = options.k
	
	locations = dict((line.strip(),0) for line in open(options.locations, 'r').readlines())
	ngram_dict = createLocationTrigrams(locations)
	prepList = [' '+line.strip()+' ' for line in open(os.path.join(data, 'prepList.txt'), 'r')]
	freqWords = [line.strip().lower() for line in open(os.path.join(data,'freqList.txt'), 'r')]

	fout = open(options.output, 'w')

	#print ngram_dict, len(ngram_dict)
	line_count = 0
	for targetString in open(options.input, 'r'):
		targetOriginal = targetString
		targetString = targetString.strip()
		targetString = targetString.lower()
			
		#remove most freq words from the string (to avoid unnecessary ngrams)
		targetString = filterString(targetString, freqWords)			
		target_ngram_locations = getTargetNgramLocations(targetString, ngram_dict)
		items = target_ngram_locations.items()		
		items.sort(key = lambda x : x[1], reverse = True)

		#if no specific ngrams are found then all the locations are searched for patterns
		if len(target_ngram_locations) >0:
			matched_strings = smithmn.check_smith_waterman(items[:LOC_COUNT], targetString, N)
		else:
			items = locations.items()
	                items.sort(key = lambda x : x[1], reverse = True)
			matched_strings = smithmn.check_smith_waterman(items, targetString, N-1)

		#get aligned complete location
		getOriginalFromMapped(matched_strings, targetString)

		#calulate levenshtien and min weighted edit distance for all matched strings
		distance_measure={}
		possible_locations = {}
		for location in matched_strings:
			weighted = distance(location, matched_strings[location], True)
			simple = distance(location, matched_strings[location])
			#distance_measure[location+'_'+matched_strings[location]] = (simple, weighted)
			distance_measure[location+'_'+ matched_strings[location]] = (weighted, simple)
			#print location, matched_strings[location], weighted, simple

		# only top 20 sorted based on levenshtien and weighted edit distance algo are considered
		levenshtien_items = sorted(distance_measure.items(), key = lambda x : x[1])[:20]
		min_observed = ""
		min_predicted = ""
		for item, (score1, score2)  in levenshtien_items:				
			(predicted_location, observed_location) = item.split('_')
			if score1<=1.5 or score2<=1.5 and min_predicted == "":
				min_predicted = predicted_location
				min_observed = observed_location
			if not possible_locations.has_key(observed_location):
				possible_locations[observed_location] = (0, predicted_location)			
			(score, top_location) = possible_locations[observed_location]
			score+=1
			possible_locations[observed_location] = (score, top_location)

		#print possible_locations,"\n", levenshtien_items[:10], "\n"
		#print targetOriginal, possible_locations, "\n", levenshtien_items
		prepCheck = False
 		executeCheck = False
		if len(possible_locations)> 1:
			#ambiguity in target location string, increases the weight for location with weighted prep rule
			rule_predicted_locations = filterByPrepRules(targetOriginal.strip(), prepList)
			for pattern in rule_predicted_locations:
				if prepCheck:
					break
				for observed_location in possible_locations:
					if observed_location == pattern.lower() and not prepCheck:
						(score, predicted_location) = possible_locations[observed_location]
						executeCheck = writeToFile(fout, observed_location, predicted_location, targetOriginal)
						prepCheck = executeCheck
						break
		if len(possible_locations) == 1 and not prepCheck:			
			#no ambiguity in target location string
			observed_location = possible_locations.keys()[0]
			(score, predicted_location) = possible_locations[observed_location]
			executeCheck = writeToFile(fout, observed_location, predicted_location, targetOriginal) 

		elif len(possible_locations) >1 and not prepCheck:			
			if min_predicted !="":
				# only of the distance is <=1
				executeCheck = writeToFile(fout, min_observed, min_predicted, targetOriginal)
			else:
				# sort observed and the most frequent one
				observed_location, (score, predicted_location) = sorted(possible_locations.items(), key = lambda x : x[1], reverse = True)[0]
				executeCheck = writeToFile(fout, observed_location, predicted_location, targetOriginal)
		if not executeCheck:
			for item, (score1, score2) in levenshtien_items:
				(predicted_location, observed_location) = item.split('_')
				executeCheck = writeToFile(fout, observed_location, predicted_location, targetOriginal)
				if executeCheck:
					break
			if not executeCheck:
				print targetOriginal.strip(), possible_locations, levenshtien_items
				spl = targetString.strip().split()
				writeToFile(fout, spl[0], locations.keys()[0], targetOriginal)
	fout.close()		
