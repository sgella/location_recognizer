import numpy
import math

key_positions = {'q':(1, 1), 'w':(1, 2), 'e':(1, 3), 'r':(1, 4), 't':(1, 5), 'y':(1, 6), 'u':(1, 7), 'i':(1, 8), 'o':(1, 9), 'p':(1, 10), 'a':(2, 1), 's':(2, 2), 'd':(2, 3), 'f':(2, 4), 'g':(2, 5), 'h':(2, 6), 'j':(2, 7), 'k':(2, 8), 'l':(2, 9), 'z':(3, 1), 'x':(3, 2), 'c':(3, 3), 'v':(3, 4), 'b':(3, 5), 'n':(3, 6), 'm':(3, 7)}

def querty_distance(a,b):
    '''distance between keys in the keyboard'''
    if not key_positions.has_key(a) or not key_positions.has_key(b):
        return 2.0
    (x1, y1) = key_positions[a] 
    (x2, y2) = key_positions[b] 
    dist =  math.sqrt ((x2-x1) ** 2 + (y2-y1) ** 2)
    return 1.0 - 1.0/(1 + dist)

def c_Lev(a,b, querty=False):
    '''Unit cost for insertions, deletions, and substitutions.'''
    assert len(a) <= 1 and len(b) <= 1
    assert not (a == '' and b == '')
    if a == b:
       return 0
    else:
      if a == '' or b == '':
          return 1
      elif querty:
          return querty_distance(a,b)
      else:
          return 2

def c_indel(a,b):
    '''Unit cost for insertions and deletions.  Substiutions are
    effectively disallowed.'''
    assert len(a) <= 1 and len(b) <= 1
    assert not (a == '' and b == '')
    if a == b:
       return 0
    elif a == '' or b == '':
       return 1
    else:
       return 2

def distance_simple(x, y, c=c_Lev):
    '''Straightforward dynamic programming method for calculating
    weighted edit distance (Levenshtein distance by default).
    Requires quadratic time and space.'''

    m = len(x)
    n = len(y)

    d = numpy.zeros((m+1,n+1))

    for i in xrange(m+1):
       for j in xrange(n+1):
          if i > 0 and j > 0:
             d[i,j] = min(d[i-1,j]   + c(x[i-1], ''),
                        d[i,  j-1] + c('',    y[j-1]),
                        d[i-1,j-1] + c(x[i-1], y[j-1]))
          elif i > 0:
             d[i,j] = d[i-1,j]   + c(x[i-1], '')
          elif j > 0:
             d[i,j] = d[i,  j-1] + c('',    y[j-1])
    #print d
    return d[m,n]

def distance(x, y, querty=False, c=c_Lev):
    '''Dynamic programming method for calculating weighted edit
    distance (Levenshtein distance by default).  Requires quadratic
    time and linear space.'''

    m = len(x)
    n = len(y)
    if m > n:
       x,y = y,x
       m,n = n,m
    assert len(x) == m and m <= n and n == len(y)

    # allocate two vectors instead of a whole matrix
    d1 = numpy.zeros(m+1)
    d2 = numpy.zeros(m+1)

    # initialize the first vector
    for i in xrange(1, m+1):
       d1[i] = d1[i-1] + c(x[i-1], '', querty)

    for j in xrange(n):
       y_j = y[j]
       #print d1
       # update the second vector
       d2[0] = d1[0] + c('', y_j, querty)
       for i in xrange(1, m+1):
          d2[i] = min(d2[i-1] + c(x[i-1], '', querty),
                    d1[i]   + c('', y_j, querty),
                    d1[i-1] + c(x[i-1], y_j, querty))
       d1,d2 = d2,d1

    #print d1
    return d1[-1]

if __name__ == "__main__":
    print "cijapur", "bijapur", distance("cijapur", "bijapur");
    print "cijapur", "bijapur", distance("cijapur", "bijapur", querty=True);
    print "cijapur", "vijapur", distance("cijapur", "vijapur", querty = True);
    print "jamalpur", "janalpur", distance("jamalpur", "janalpur");
